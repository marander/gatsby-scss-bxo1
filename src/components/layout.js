/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql, Link } from "gatsby"

import Header from "./header"
import "../../static/styles/main.scss"
import logo from "../../static/images/logo.svg"
import iconTwitter from "../../static/images/icon-twitter.svg"
import iconLinkedin from "../../static/images/icon-linkedin.svg"
import iconGitHub from "../../static/images/icon-github.svg"
import iconInstagram from "../../static/images/icon-instagram.svg"

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Header siteTitle={data.site.siteMetadata.title} />
        <main>{children}</main>
        <footer>
          <div className={"container"}>
            <div className={"row"}>
              <div className={"col-5"}>
                <div className={"widget__item"}>
                  <div className={"logo"}>
                    <Link to="/" title={"BotXo"}>
                      <img alt={"Logo"} src={logo} />
                    </Link>
                  </div>

                  <div className={"about"}>
                    <p>
                    Working with chatbots, voice bots, NLU, Machine Learning & AI.
Building software for the conversational era.
We named our company “Bot-X-O” because chatbots, voice bots and digital assistants running on our technology are not only improving user experiences and transforming organisations, but also clearing the way for more meaningful interactions between companies, their employees and customers.

We are on a mission to spearhead this evolution. 
                    </p>
                  </div>
                </div>
              </div>

              <div className={"col-2"}>
                <div className={"widget__item"}>
                  <ul className={"links"}>
                    <h4>BotXO</h4>
                    <ul>
                      <li>
                        <Link to="/integrations/slack" title={"Integrations"}>
                          Integrations
                        </Link>
                      </li>
                      <li>
                        <Link to="/about" title={"About Us"}>
                          About
                        </Link>
                      </li>
                      <li>
                        <Link to="/blog" title={"Blog and News section"}>
                          Blog and news
                        </Link>
                      </li>
                      <li>
                        <Link to="/joinus" title={"Join us"}>
                          Jobs
                        </Link>
                      </li>
                      <li>
                        <Link to="/partners" title={"partners"}>
                          Partners
                        </Link>
                      </li>
                      <li>
                        <a
                          className={"links__special"}
                          href={"https://botxo.ai/feedback/"}
                          target={"_blank"}
                          title={
                            "We look forward to receiving your great feedback"
                          }
                        >
                          Feedback
                        </a>
                      </li>
                    </ul>
                  </ul>
                </div>
              </div>

              <div className={"col-2"}>
                <div className={"widget__item"}>
                  <div className={"links"}>
                    <h4>Support</h4>
                    <ul>
                      <li>
                        <Link to="/contact" title={"Contact Us"}>
                          Contact
                        </Link>
                      </li>
                      <li>
                        <Link to="/privacy" title={"Privacy Policy"}>
                          Privacy
                        </Link>
                      </li>
                      <li>
                        <Link to="/datapolicy" title={"Data Policy"}>
                          Data Policy
                        </Link>
                      </li>
                      <li>
                        <Link to="/terms" title={"Terms Of Use"}>
                          Terms Of Use
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <div className={"col-3"}>
                <div className={"widget__item"}>
                  <div className={"social"}>
                    <a
                      href="https://twitter.com/botxohumans"
                      target={"_blank"}
                      title={"Twitter"}
                    >
                      <img alt={"Twitter"} src={iconTwitter} />
                    </a>
                    <a
                      href="https://www.linkedin.com/company/botxo/"
                      target={"_blank"}
                      title={"LinkedIn"}
                    >
                      <img alt={"LinkedIn"} src={iconLinkedin} />
                    </a>
                    <a
                      href="https://github.com/webmeaistro"
                      target={"_blank"}
                      title={"GitHub"}
                    >
                      <img alt={"GitHub"} src={iconGitHub} />
                    </a>
                    <a
                      href="https://www.instagram.com/botxohumans/"
                      target={"_blank"}
                      title={"Instagram"}
                    >
                      <img alt={"Instagram"} src={iconInstagram} />
                    </a>
                  </div>
                </div>
              </div>
            </div>

            <div className={"copyright"}>
              <p>
                Copyright {new Date().getFullYear()}, {` `}{" "}
                <a href="https://botxo.ai" title={"BotXo"}>
                  BotXO
                </a>
                . All rights reserved.| Contact: hello@botxo.co or +45 26 71 58 45
              </p>
            </div>
          </div>
        </footer>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
